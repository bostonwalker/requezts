import libzt
import pytest

from requezts import _socket

# noinspection PyUnresolvedReferences
from utils import read_queue

# noinspection PyUnresolvedReferences
from testnode import TestNode

# noinspection PyUnresolvedReferences
from testserver import TestServer


class TestSockets:

    node = TestNode(TestServer().net_id)

    @classmethod
    def setup_class(cls):
        cls.node.start()

    @classmethod
    def teardown_class(cls):
        cls.node.stop()

    def test_sockets_timeout(self):
        """Test behaviour with settimeout() and gettimeout()"""

        sock = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)

        # assert sock.gettimeout() == sock.getdefaulttimeout()  # TODO

        sock.settimeout(7.5)
        assert sock.gettimeout() == 7.5
        assert not sock.getblocking()

        sock.settimeout(0.001)
        assert sock.gettimeout() == 0.001
        assert not sock.getblocking()

        sock.settimeout(0.)
        assert sock.gettimeout() == 0.
        assert not sock.getblocking()

        sock.settimeout(None)
        assert sock.gettimeout() is None
        assert sock.getblocking()

        with pytest.raises(Exception):
            sock.settimeout(-1.)

    def test_socket_send_invalid_object(self):
        """Pass invalid buffer to send() - ensure error-handling code is working correctly"""
        sock = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        with pytest.raises(SystemError):
            sock.send(123)

    def test_socket_sendall_invalid_object(self):
        """Pass invalid buffer to sendall() - ensure error-handling code is working correctly"""
        sock = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        with pytest.raises(SystemError):
            sock.sendall(123)

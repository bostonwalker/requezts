import pytest
from requests.exceptions import InvalidSchema, MissingSchema, InvalidURL

from requezts import Session


class TestSessions:

    session = Session(net_id=0xb6079f73c6c6f1af)

    @classmethod
    def setup_class(cls):
        cls.session.open()

    @classmethod
    def teardown_class(cls):
        cls.session.close()

    def test_open_active_session_throws_error(self):
        with pytest.raises(AssertionError):
            self.session.open()

    def test_close_inactive_session_throws_error(self):
        with pytest.raises(AssertionError):
            Session(net_id=0x0123456789abcdef).close()

    @pytest.mark.parametrize(
        'exception, url', (
            (MissingSchema, 'hiwpefhipowhefopw'),
            (InvalidSchema, 'localhost:3128'),
            (InvalidSchema, 'localhost.localdomain:3128/'),
            (InvalidSchema, '10.122.1.1:3128/'),
            (InvalidURL, 'http://'),
        ))
    def test_invalid_url(self, exception, url):
        with pytest.raises(exception):
            self.session.get(url)

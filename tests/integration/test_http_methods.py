from typing import Optional

import json

from requests import Request
from starlette import status
from starlette.responses import Response, PlainTextResponse

from requezts import Session

# noinspection PyUnresolvedReferences
from utils import request_to_bytes, response_from_bytes, read_queue

# noinspection PyUnresolvedReferences
from testserver import TestServer


class TestHTTPMethods:

    server: Optional[TestServer] = None
    session: Optional[Session] = None

    @classmethod
    def setup_class(cls):

        cls.server = TestServer()

        @cls.server.route("/hello/", "GET")
        def _hello_get(_: Request) -> Response:
            return PlainTextResponse(status_code=status.HTTP_200_OK, content="Hello, World!")

        @cls.server.route("/hello/", "POST")
        def _hello_post(request: Request) -> Response:
            name = json.loads(request.json)['name']
            return PlainTextResponse(status_code=status.HTTP_200_OK, content=f"Hello, {name}!")

        cls.server.start()

        cls.session = Session(net_id=cls.server.net_id)
        cls.session.open()

    @classmethod
    def teardown_class(cls):
        cls.server.stop()
        cls.session.close()

    def test_get_helloworld(self):
        """Test GET method using Session"""
        response = self.session.get(f'http://{self.server.ip_addr}:{self.server.port}/hello/')
        assert response.text == 'Hello, World!'

    def test_post_helloworld(self):
        """Test POST method using Session"""
        response = self.session.post(f'http://{self.server.ip_addr}:{self.server.port}', json={'name': 'Jack'})
        assert response.text == 'Hello, Jack!'

import json

import pytest
import requests
from requests import Request
from starlette import status
from starlette.responses import Response, PlainTextResponse, JSONResponse

import libzt

from requezts import _socket, _SocketIO

# noinspection PyUnresolvedReferences
from utils import request_to_bytes, response_from_bytes, read_queue

# noinspection PyUnresolvedReferences
from testnode import TestNode

# noinspection PyUnresolvedReferences
from testserver import TestServer


class TestSocketReadWrite:

    server: TestServer = TestServer()
    node: TestNode = TestNode(server.net_id)

    @classmethod
    def setup_class(cls):

        @cls.server.route("/hello/", "GET")
        def _hello_get(_: Request) -> Response:
            return PlainTextResponse(status_code=status.HTTP_200_OK, content="Hello, World!")

        @cls.server.route("/echo/", "PUT")
        def _echo_put(request: Request) -> Response:
            return JSONResponse(status_code=status.HTTP_200_OK, content=json.loads(request.json))

        cls.server.start()
        cls.node.start()

    @classmethod
    def teardown_class(cls):
        cls.node.stop()
        cls.server.stop()

    def test_socket_read_write(self):
        """Basic test of sending messages between sockets to confirm that everything is working correctly in libzt"""

        client = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        client.settimeout(10.)

        client.connect((self.server.ip_addr, self.server.port))

        print('Client connected to server')

        host = f"{self.server.ip_addr}:{self.server.port}"
        request = Request(method="GET", url=f"http://{host}/hello/", headers={"Host": host})
        data_tx = request_to_bytes(request)
        client.send(data_tx)

        print(f'Client sent data: {data_tx}')

        data_rx = client.recv(1024)

        print(f'Client received data: {data_rx}')

        client.close()

        print('Client disconnected from server')

        response = response_from_bytes(data_rx)

        assert response.status_code == status.HTTP_200_OK
        assert response.body.decode("UTF-8") == "Hello, World!\n"

    def test_socket_send_large_payload(self):
        """Test sending a large payload using send()"""

        data = b"." * 1048576  # One *mega*-byte

        client = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        client.connect((self.server.ip_addr, self.server.port))
        client.send(data)
        client.close()

    def test_socket_sendall_large_payload(self):
        """Test sending a large payload using sendall()"""

        data = b"." * 1048576  # One *mega*-byte

        client = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        client.connect((self.server.ip_addr, self.server.port))
        client.sendall(data)
        client.close()

    def test_socket_sendall_timeout(self):
        """Test that timeout is triggered when sending a large amount of data with a short timeout"""

        bytes = b"." * 1048576  # One *mega*-byte

        client = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        client.connect((self.server.ip_addr, self.server.port))

        client.settimeout(0.001)

        with pytest.raises(TimeoutError):
            client.sendall(bytes)

    def test_socketio_read_write(self):
        """Test read/write using _SocketIO construct over underlying libzt socket"""
        # John 3:16 in Cherokee
        resp = requests.get("https://bible-api.com/" +
                            "%E1%8E%A3%E1%8F%8D%E1%8F%9B%20%E1%8E%A7%E1%8F%83%E1%8E%AE%E1%8F%9B%20%E1%8F%A3%E1%8F%82" +
                            "%20%E1%8E%A4%E1%8F%AC%E1%8F%AA%E1%8E%B3%E1%8F%85%E1%8E%AF+3:16?translation=cherokee")
        json_tx = resp.json()
        json_tx["text"] = "".join(json_tx["text"] for _ in range(1000))  # Enlarge data

        client = _socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
        client.settimeout(10.)

        client.connect((self.server.ip_addr, self.server.port))

        print('Client connected to server')

        _io = _SocketIO(client, "rwb")

        host = f"{self.server.ip_addr}:{self.server.port}"
        request = Request(method="PUT", url=f"http://{host}/echo/",
                          headers={"Host": host, "Content-Type": "application/json"},
                          json=json.dumps(json_tx))
        data_tx = request_to_bytes(request)
        _io.write(data_tx)

        print(f'Client sent data: {data_tx}')

        data_rx = _io.readall()

        print(f'Client received data: {data_rx}')

        client.close()

        print('Client disconnected from server')

        response = response_from_bytes(data_rx)
        assert response.status_code == status.HTTP_200_OK

        print(repr(response.body))
        json_rx = json.loads(response.body.decode('UTF-8'))
        assert json_rx == json_tx

from typing import Callable, MutableMapping, Tuple, Optional, Mapping

import sys
from multiprocessing import Queue, Process
import dill

from urllib.parse import urlparse
from requests import Request
from starlette.responses import Response

import libzt

from utils import read_queue, request_from_bytes, response_to_bytes
from testnode import TestNode


Route = str
Method = str
Handler = Callable[[Request], Response]


class TestServer:
    """
    Asynchronous server process manager

    :param net_id: ZeroTier net_id
    :param port: Port to bind to
    """

    def __init__(self, net_id: int = 0xb6079f73c6c6f1af, port: int = 8000):

        self.__net_id = net_id
        self.__port = port

        self.__handlers: MutableMapping[Tuple[Route, Method], Handler] = {}
        self.__queue = Queue()

        self.__process: Optional[Process] = None
        self.__ip_addr: Optional[str] = None

    @property
    def net_id(self) -> int:
        """ZeroTier net_id"""
        return self.__net_id

    @property
    def port(self) -> int:
        """Port to listen on"""
        return self.__port

    @property
    def ip_addr(self) -> Optional[str]:
        """IP address server is currently listening on"""
        return self.__ip_addr

    def route(self, route: str, method: str):
        """
        Decorator for registering a Handler function for a route/method

        :param route: URL route (e.g. "/")
        :param method: HTTP method (e.g. "GET")
        """
        def _wrapper(func: Handler) -> Handler:
            self.__handlers[(route, method)] = func
            return func

        return _wrapper

    def clear_routes(self):
        """
        Clear all registered route handler functions
        """
        self.__handlers.clear()

    def start(self, timeout: float = 60.):
        """
        Start the server (connect to ZeroTier net and bind to port)

        :param timeout: Time to wait for server to come online
        :return: IP address"""

        assert self.__process is None, 'cannot start server twice'

        self.__process = Process(
            target=TestServer._listen,
            args=(self.__queue, self.__net_id, self.__port, dill.dumps(self.__handlers)),
        )

        self.__process.start()

        ip_addr: str = read_queue(self.__queue, block=True, timeout=timeout)
        assert ip_addr is not None

        self.__ip_addr = ip_addr

    def stop(self, timeout: float = 5.):
        """
        Stop the server

        :param timeout: Time to wait for server to come offline
        """
        assert self.__process is not None, "server must be started"

        self.__process.terminate()
        self.__process.join(timeout=timeout)
        print('Terminated server')

        self.__process = None
        self.__ip_addr = None

        # Raise any remaining exceptions
        if not self.__queue.empty():
            read_queue(self.__queue, block=False)

    @staticmethod
    def _listen(_queue: Queue, net_id: int, port: int, handlers: str) -> None:
        """Callable for server process"""

        handlers: Mapping[Tuple[Route, Method], Handler] = dill.loads(handlers)

        try:
            with TestNode(net_id) as node:

                print(f'Server online')

                serv = libzt.socket(libzt.ZTS_AF_INET, libzt.ZTS_SOCK_STREAM, 0)
                # serv.settimeout(30.)

                # Bind to port
                serv.bind(('0.0.0.0', port))
                serv.listen(1)

                # Get ip address and return to main process
                ip_addr = node.addr_get_ipv4(net_id)
                host = f"{ip_addr}:{port}"

                print(f'Server listening on {host}')

                _queue.put(ip_addr)

                # Handle incoming connections
                while True:
                    conn, addr = serv.accept()
                    print(f'Server accepted connection from {addr}')

                    bytes_rx = conn.recv(4096)
                    print('Server received data')

                    # Parse incoming bytes as request
                    request = request_from_bytes(bytes_rx)
                    url = urlparse(request.url)

                    # Get handler
                    handler = handlers[(url.path, request.method)]

                    # This is where the magic happens
                    response: Response = handler(request)

                    # Transmit data
                    bytes_tx = response_to_bytes(response)
                    conn.send(bytes_tx)
                    print(f'Server sent data')

                    conn.close()
                    print('Server disconnected')

        except Exception as e:
            print('Server exception!')
            _queue.put((e, sys.exc_info()))
            raise e

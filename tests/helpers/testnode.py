from time import sleep

import libzt.node


class TestNode(libzt.node.ZeroTierNode):
    """
    Convenience context manager class for managing ZeroTier node state

    :param net_id: ZeroTier network ID
    """
    def __init__(self, net_id: int):
        super().__init__()
        self.__net_id = net_id

        # Improve performance when connecting to self
        libzt.zts_init_allow_net_cache(True)
        libzt.zts_init_allow_peer_cache(True)
        libzt.zts_init_allow_roots_cache(True)

    @property
    def net_id(self) -> int:
        return self.__net_id

    def start(self):
        """Start node and join net"""
        self.node_start()
        while not self.node_is_online():
            sleep(0.01)
        self.net_join(self.net_id)
        while not self.net_transport_is_ready(self.net_id):
            sleep(0.01)

    def stop(self):
        """Bring node down"""
        self.node_stop()
        self.node_free()

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()
        return False

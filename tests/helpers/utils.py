from typing import Optional, Any

import queue

from http import HTTPStatus
from urllib.parse import urlparse
from email.parser import BytesParser
from requests import Request
from starlette.responses import Response, JSONResponse, HTMLResponse, PlainTextResponse


def request_to_bytes(request: Request, encoding: Optional[str] = None) -> bytes:
    """Convert requests.Request to HTTP/1.1 request bytes"""

    encoding = encoding or "UTF-8"

    url = urlparse(request.url)

    request_str = f"{request.method} {url.path} HTTP/1.1\r\n" +\
                  "\r\n".join([f"{k}: {v}" for k, v in request.headers.items()]) + "\r\n" +\
                  "\r\n"

    body = request.data or request.json
    if body:
        request_str += body + "\n"

    return request_str.encode(encoding)


def request_from_bytes(_bytes: bytes, encoding: Optional[str] = None) -> Request:
    """Parse bytes received as HTTP/1.1 request"""

    encoding = encoding or "UTF-8"

    # Separate request into metadata
    try:
        request_bytes, body_bytes = _bytes.split(b"\r\n\r\n", maxsplit=1)
        if len(body_bytes) == 0:
            raise ValueError
        has_body = True
    except ValueError:
        request_bytes = _bytes
        body_bytes = None
        has_body = False

    # Parse request
    request_line_bytes, headers_bytes = request_bytes.split(b"\r\n", maxsplit=1)
    headers = BytesParser().parsebytes(headers_bytes)

    method, resource, proto = request_line_bytes.decode(encoding).split()

    # Request line
    assert proto == "HTTP/1.1"

    # Headers
    assert "Host" in headers
    host = headers["Host"]
    url = f"http://{host}{resource}"

    data: Optional[Any] = None
    json: Optional[Any] = None

    # Parse body
    if has_body:
        assert method in ["POST", "PUT"], f"{method} requests should not have a body"
        body = body_bytes.decode("UTF-8") if body_bytes else None
        content_type = headers.get_content_type()
        if content_type == "application/json":
            json = body
        elif content_type == "multipart/form-data":
            data = body
        else:
            raise NotImplementedError(f"Content-Type not understood: {content_type}")

    return Request(method=method, url=url, headers=headers, files=None, data=data, params=None, auth=None,
                   cookies=None, hooks=None, json=json)


def response_to_bytes(response: Response) -> bytes:
    """Convert starlette Response to HTTP/1.1 response bytes"""

    response_str = f"HTTP/1.1 {response.status_code} {HTTPStatus(response.status_code).name}\r\n" +\
                   "\r\n".join([f"{k}: {v}" for k, v in response.headers.items()]) + "\r\n" +\
                   "\r\n"

    response_bytes = response_str.encode(response.charset)

    if response.body:
        response_bytes += response.body + b"\n"

    return response_bytes


def response_from_bytes(_bytes: bytes, encoding: Optional[str] = None) -> Response:
    """Convert HTTP/1.1 response bytes to starlette Response"""

    encoding = encoding or "UTF-8"

    # Separate response into metadata and body
    try:
        response_bytes, body_bytes = _bytes.split(b"\r\n\r\n", maxsplit=1)
        has_body = True
    except ValueError:
        response_bytes = _bytes
        body_bytes = None
        has_body = False

    # Parse response
    response_line_bytes, headers_bytes = response_bytes.split(b"\r\n", maxsplit=1)
    proto, status_code, status_text = response_line_bytes.decode(encoding).split()
    headers = BytesParser().parsebytes(headers_bytes)

    # Request line
    assert proto == "HTTP/1.1"

    content: Optional[Any] = None

    # Parse body
    if has_body:
        content = body_bytes.decode("UTF-8") if body_bytes else None
        content_type = headers.get_content_type()
        if content_type == "application/json":
            response_cls = JSONResponse
        elif content_type == "text/html":
            response_cls = HTMLResponse
        elif content_type == "text/plain":
            response_cls = PlainTextResponse
        else:
            raise NotImplementedError(f"unknown content type: {content_type}")
    else:
        response_cls = Response

    return response_cls(status_code=int(status_code), headers=dict(headers), content=content)


def read_queue(q: queue.Queue, block: bool = True, timeout: float = 0.) -> Optional[Any]:
    """Read queue. If queue is empty, return None. If queue returns an exception, raise it."""
    try:
        result: Optional[Any] = q.get(block=block, timeout=timeout)
    except queue.Empty:
        if block:
            raise TimeoutError
        else:
            result = None
    if isinstance(result, Exception):
        raise result
    else:
        return result

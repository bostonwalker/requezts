## requezts 0.0.3

### New features:

None

### Bug fixes:

- Fix issue import TestServer
- Fix erroneous import from urllib3.connection causing incompatibility with "strict" keyword

### Future work (roadmap to v1.0.0):

- Address speed and reliability of connections (due to underlying libzt issues)
- Add support for HTTPS
- Expand testing to ensure production-readiness

## requezts 0.0.2

### New features:

Sphinx docs: https://requezts.readthedocs.io/en/latest/

### Bug fixes:
None

### Future work (roadmap to v1.0.0):

- Address speed and reliability of connections (due to underlying libzt issues)
- Add support for HTTPS
- Expand testing to ensure production-readiness

## requezts 0.0.1

Initial release - now available on PyPI!
This project is currently in the alpha stage

### New features:

HTTP/1.1 requests (GET, POST, PUT, etc...) over basic HTTP (HTTPS not yet supported)

###  Bug fixes:

None

### Future work (roadmap to v1.0.0):
- **Address speed and reliability of connections (due to underlying libzt issues)**
- Add support for HTTPS
- Add sphinx documentation
